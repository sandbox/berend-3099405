module Page.Articles exposing
    ( Model
    , Msg
    , init
    , view
    , update
    )


import Html exposing (Html, text, a, div, p)
import Http
import HttpBuilder
import RemoteData exposing (RemoteData(..), WebData)
import JsonApi.Article as JsonApi
import Route
import Session exposing (Session)


-- MODEL

type alias Model =
    { articles : WebData ( List JsonApi.Data )
    }


defaultModel : Model
defaultModel =
    { articles = NotAsked
    }


init : Session -> ( Model, Msg )
init session =
    ( defaultModel, GetArticles )



-- UPDATE

type Msg
    = GetArticles
    | GotArticles (Result Http.Error (JsonApi.Articles))


update : Msg -> Model -> ( Model, Cmd Msg, Maybe Session.Msg )
update msg model =
    case msg of
        GetArticles ->
            ( { model | articles = Loading }, getArticles, Nothing )

        GotArticles (Ok json) ->
            ( { model | articles = Success json.data }, Cmd.none, Nothing )

        GotArticles (Err err) ->
            ( { model | articles = Failure err }, Cmd.none, Nothing )




-- VIEW

view : Model -> Html msg
view model =
    case model.articles of
        NotAsked ->
            p [] [ text ""]

        Loading ->
            p [] [ text "Spinner here"]

        Success articles ->
            div []
                ( List.map
                    (\item ->
                         p []
                             [ a [ Route.href ( Route.Article item.id ) ]
                                   [ text item.attributes.title ]
                             ]
                    )
                    articles )

        Failure _ ->
            p [] [ text "Error loading articles"]



-- FETCH

getArticles =
    HttpBuilder.get ("/jsonapi/node/article")
        |> HttpBuilder.withExpect (Http.expectJson GotArticles JsonApi.decodeArticles)
        |> HttpBuilder.request
