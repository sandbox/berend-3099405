module Page.Article exposing
    ( Model
    , Msg
    , init
    , view
    , update
    )


import Html exposing (Html, text, div, p)
import Http
import HttpBuilder
import RemoteData exposing (RemoteData(..), WebData)
import JsonApi.Article as JsonApi
import Session exposing (Session)


-- MODEL

type alias Model =
    { article : WebData JsonApi.Data
    }


defaultModel : Model
defaultModel =
    { article = NotAsked
    }


init : Session -> String -> ( Model, Msg )
init session uuid =
    ( defaultModel, GetArticle uuid )



-- UPDATE

type Msg
    = GetArticle String
    | GotArticle (Result Http.Error (JsonApi.Article))


update : Msg -> Model -> ( Model, Cmd Msg, Maybe Session.Msg )
update msg model =
    case msg of
        GetArticle uuid ->
            ( { model | article = Loading }, getArticle uuid, Nothing )

        GotArticle (Ok json) ->
            ( { model | article = Success json.data }, Cmd.none, Nothing )

        GotArticle (Err err) ->
            ( { model | article = Failure err }, Cmd.none, Nothing )




-- VIEW

view : Model -> Html msg
view model =
    case model.article of
        NotAsked ->
            p [] [ text ""]

        Loading ->
            p [] [ text "Spinner here"]

        Success article ->
            p [] [ text article.attributes.body.processed ]

        Failure _ ->
            p [] [ text "Error loading article"]



-- FETCH

getArticle uuid =
    HttpBuilder.get ("/jsonapi/node/article/" ++ uuid)
        |> HttpBuilder.withExpect (Http.expectJson GotArticle JsonApi.decodeArticle)
        |> HttpBuilder.request
