module Page exposing
    ( Page(..)
    )


import Errored exposing (PageLoadError)

import Page.Hello
import Page.Articles
import Page.Article

type Page
    = Loading
    | Hello Page.Hello.Model
    | Articles Page.Articles.Model
    | Article Page.Article.Model
    | Errored PageLoadError
