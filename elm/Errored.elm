module Errored exposing (PageLoadError, pageLoadError, view)

import Html exposing (Html, text)


type PageLoadError
    = PageLoadError Model


type alias Model =
    { errorMessage : String }


pageLoadError : String -> PageLoadError
pageLoadError errorMessage =
    PageLoadError { errorMessage = errorMessage }


view : PageLoadError -> Html msg
view (PageLoadError model) =
    text model.errorMessage
