"use strict";

import "../scss/app.scss"
import "../elm-mdc/src/elm-mdc.js"

const {Elm} = require('../elm/Main.elm')


var loadjs = require('loadjs')

loadjs(["css!https://fonts.googleapis.com/icon?family=Material+Icons"], {
  success: function () {
    // Initialise Elm
    const flags = null
    Elm.Main.init({node: document.getElementById("main"), flags: flags})
  }
})
